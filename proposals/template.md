* Name: (provide a unique name, my_feature)
* Start Date: (today's date, YYYY-MM-DD)

# Summary

One paragraph explaining the idea.

# Motivation

Why is this important?

# Detail

Describe the protocol in detail.

# Open Questions

What still needs to be decided?
